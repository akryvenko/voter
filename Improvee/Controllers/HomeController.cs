﻿using System.Web.Mvc;

namespace Improvee.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Voter";

            return View();
        }
    }
}