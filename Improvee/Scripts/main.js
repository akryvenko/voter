﻿var app = angular.module("voteApp");
app.controller("voteController", ["$scope", "voteService", function ($scope, voteService) {

    voteService.getQuiz().then(function (result) {
        $scope.questions = result.data.VoteQuestions;
        $scope.isOrdered = result.data.IsOrdered;
    }, function(err) {
        alert(err);
    });

    var currentIndex = 1;
    $scope.stages = ["start", "QA", "end"];
    $scope.curStage = $scope.stages[currentIndex];
    $scope.isAnonimus = false;
    $scope.email = "";
    $scope.nextStage = function () {
        $scope.curStage = $scope.stages
            [++currentIndex % $scope.stages.length];
    };
    $scope.isNotStart = function() {
        return $scope.isAnonimus && !$scope.email;
    };
    $scope.close = function() {
        $scope.nextStage();
        //window.close();
    };
    $scope.isShowNextQuestion = function (ind) {
        if (ind == 0)
            return true;

        for (var i = 0; i < ind; i++) {
            if (!$scope.questions[i].Answer)
                return false;
        }
        return true;
    };
    $scope.isShowSendResponse = function () {
        if (!$scope.questions)
            return false;
        for (var i = 0; i < $scope.questions.length; i++) {
            if (!$scope.questions[i].Answer)
                return false;
        }
        return true;
    };
}]);