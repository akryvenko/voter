﻿namespace Improvee.Models
{
    using System.Collections.Generic;
    using System.Linq;

    public class VoteResponse
    {
        public int QuestionOrder { get; set; }
        public string Answer { get; set; }
    }

    public class VoteQuestion
    {
        public string Question { get; set; }

        public int Order { get; set; }

        public SelectVoteType SelectVoteType { get; set; }

        public List<string> Options { get; set; }

        public VoteQuestion(string question, int order, SelectVoteType selectVoteType, List<string> options)
        {
            Question = question;
            Order = order;
            SelectVoteType = selectVoteType;
            Options = options;
        }

        public VoteQuestion(string question, int order, SelectVoteType selectVoteType, params string[] options)
        : this(question, order, selectVoteType, options.ToList()) { }
    }

    public class Quiz
    {
        public bool IsOrdered { get; set; }
        public List<VoteQuestion> VoteQuestions { get; set; }

        public Quiz(bool isOrdered)
        {
            IsOrdered = isOrdered;
            VoteQuestions = new List<VoteQuestion>();
        }
    }

    public class QuizBuilder
    {
        public Quiz Quiz { get; set; }

        public QuizBuilder()
        {
            Quiz = new Quiz(true);
        }

        public QuizBuilder AddInput(string question)
        {
            Quiz.VoteQuestions.Add(new VoteQuestion(question,
                Quiz.VoteQuestions.Count + 1, SelectVoteType.Input));
            return this;
        }
        public QuizBuilder AddYesNo(string question)
        {
            Quiz.VoteQuestions.Add(new VoteQuestion(question,
                Quiz.VoteQuestions.Count + 1, SelectVoteType.Radio, "yes", "no"));
            return this;
        }
        public QuizBuilder AddOptions(string question, params string[] options)
        {
            Quiz.VoteQuestions.Add(new VoteQuestion(question,
                Quiz.VoteQuestions.Count + 1, SelectVoteType.Checkbox, options));
            return this;
        }
    }
}