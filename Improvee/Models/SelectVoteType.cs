﻿namespace Improvee.Models
{
    public enum SelectVoteType
    {
        Dopdown = 1,
        Radio = 2,
        Checkbox = 3,
        Input = 4
    }
}