﻿using System.Collections.Generic;
using System.Web.Http;

namespace Improvee.Controllers
{
    using Models;

    //[Authorize]
    public class QuizController : ApiController
    {
        // GET api/Quiz
        public Quiz Get()
        {
            return new QuizBuilder()
                .AddInput("What is your favorite book")
                .AddOptions("Your favorite products", "banana", "apple", "beer", "meat")
                .AddYesNo("Do you like playing guitar")
                .AddInput("What is your favorit band")
                .Quiz;
        }

        // GET api/Quiz/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/Quiz
        public void Post([FromBody]string value)
        {
        }

        // PUT api/Quiz/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/Quiz/5
        public void Delete(int id)
        {
        }
    }
}
